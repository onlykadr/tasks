import { AppTasksPage } from './app.po';

describe('app-tasks App', () => {
  let page: AppTasksPage;

  beforeEach(() => {
    page = new AppTasksPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
