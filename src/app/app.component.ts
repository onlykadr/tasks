import { Task } from './shared/task';
import { tasks } from './shared/data';
import { Component } from '@angular/core';


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  tasks: Task[] = tasks;
}
