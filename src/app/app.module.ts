import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { TasksFormComponent } from './tasks-form/tasks-form.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksItemComponent } from './tasks-item/tasks-item.component';

import { TasksService } from './shared/tasks.service';

import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    TasksFormComponent,
    TasksListComponent,
    TasksItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [TasksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
