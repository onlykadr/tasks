import { Injectable } from '@angular/core';
import { tasks } from './data';
import { Task } from './task';


@Injectable() // для возможности использовать другие сервисы
export class TasksService {
  tasks: Task[] = tasks;

  getTasks(): Task[] {
    return this.tasks;
  }

  onCreateTask(title: string) {
    const task: Task = new Task(title);

    this.tasks.splice(0, 0, task);
  }

  onDeleteTask(task: Task) {
    const index = this.tasks.indexOf(task);

    if (index > -1) {
      this.tasks.splice(index, 1);
    }
  }

  onEditTask(task: Task) {
    const taskTitle = task.title;
    const index = this.tasks.indexOf(task);
    
  }

  onToggleTask(task: Task) {
    task.done = !task.done;
  }
}
