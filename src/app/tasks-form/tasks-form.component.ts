import { Component, Input } from '@angular/core';
import { TasksService } from './../shared/tasks.service';

@Component({
  moduleId: module.id,
  selector: 'tasks-form',
  templateUrl: 'tasks-form.component.html'
})
export class TasksFormComponent {
  title = '';
  constructor(private tasksService: TasksService) { }

  onSubmit() {
    this.tasksService.onCreateTask(this.title);
  }
}
