import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Task } from './../shared/task';

@Component({
  moduleId: module.id,
  selector: 'tasks-item',
  templateUrl: 'tasks-item.component.html'
})
export class TasksItemComponent {
  @Input() task: Task;
  @Output() delete = new EventEmitter();
  @Output() toggle = new EventEmitter();
  @Output() edit = new EventEmitter();

  onToggle() {
    this.toggle.emit(this.task);
  }

  onEdit() {
    this.edit.emit(this.task);
  }

  onDelete() {
    this.delete.emit(this.task);
  }

}
