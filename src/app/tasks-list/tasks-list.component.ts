import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Task } from './../shared/task';
import { TasksService } from './../shared/tasks.service';

@Component({
  moduleId: module.id,
  selector: 'tasks-list',
  templateUrl: 'tasks-list.component.html'
})
export class TasksListComponent implements OnInit {
  tasks: Task[];
  @Output() xxx = new EventEmitter();

  constructor(private tasksService: TasksService) {
    this.tasks = [];
  }

  ngOnInit() {
    this.tasks = this.tasksService.getTasks();
  }

  delete(task: Task) {
    this.tasksService.onDeleteTask(task);
  }

  edit(task: Task) {

    console.log(this.tasks.indexOf(task));
    // this.tasksService.onEditTask(task);
  }

  toggle(task: Task) {
    this.tasksService.onToggleTask(task);
  }

}
